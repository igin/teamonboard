import * as React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import * as firebaseui from 'firebaseui';

import { App } from './app';

const firebaseConfig = {
  apiKey: "AIzaSyBYjdz3tDnwVUNhaUMaQmUj5RTmeqsvgeE",
  authDomain: "team-on-board.firebaseapp.com",
  databaseURL: "https://team-on-board.firebaseio.com",
  projectId: "team-on-board",
  storageBucket: "team-on-board.appspot.com",
  messagingSenderId: "128349068855",
  appId: "1:128349068855:web:d976bad73f9381224829b2"
};


const app = firebase.initializeApp(firebaseConfig);
const ui = new firebaseui.auth.AuthUI(app.auth());
ReactDOM.render(
  <App
    firebaseApp={app}
    firebaseUI={ui}
  />, document.getElementById('appContainer'));
