import * as React from 'react';
import { UserProfile, Database } from './database';
import TextField from '@material-ui/core/TextField';

interface IProps {
    firebaseApp: firebase.app.App,
    firebaseUser: firebase.User
}

export const ProfileScreen = (props: IProps) => {
    const {
        firebaseApp,
        firebaseUser
    } = props;

    const [profile, setProfile] = React.useState<UserProfile | null>(null);

    const db = new Database(firebaseApp);

    React.useEffect(() => {
        db.getOrCreateProfile(firebaseUser).then((profile) => {
            setProfile(profile);
        })
    }, []);

    return (
        <div>
            <h1>Your Profile</h1>
            {
                profile == null ? <div>Loading...</div> :
                    (<div>
                        <TextField label="Name" value={profile.name} />




                    </div>)
            }
        </div>
    )
};