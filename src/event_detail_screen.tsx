import * as React from 'react';
import { useParams, useRouteMatch, Route, Switch, Link } from 'react-router-dom';
import { Database, IdeationEvent } from './database';
import moment from 'moment';
import { IdeaBrowser } from './idea_browser';
import styled from 'styled-components';
import Brain from './images/brain.svg';
import EventIcon from '@material-ui/icons/Event';

interface IProps {
    firebaseApp: firebase.app.App;
}

const DateFormat = 'MMMM Do YYYY, h:mm a';

export const EventDetailScreen = (props: IProps) => {
    const { eventId } = useParams();
    const {
        firebaseApp
    } = props;
    const [event, setEvent] = React.useState<IdeationEvent | null>();

    const database = new Database(firebaseApp);

    const getData = (): () => void => {
        database.getEvent(eventId).then((event) => {
            setEvent(event);
        })

        const unsubscribe = () => { };
        return unsubscribe;
    };

    const match = useRouteMatch();

    React.useEffect(() => {
        getData();
    }, []);

    return (
        <div>
            {event == null ? <div>loading...</div> :
                <div>
                    <EventHeader>
                        <img src={Brain} />
                        <h1>{event.title}</h1>
                        <EventDate><EventIcon style={{ fontSize: 'medium', opacity: 0.5 }} />{moment(event.start_date.toDate()).format(DateFormat)}</EventDate>
                        <p>DISCOVER SOME IDEAS</p>
                    </EventHeader>
                    <Switch>
                        <Route path={`${match.path}`}>
                            <IdeaBrowser
                                firebaseApp={firebaseApp}
                            />
                        </Route>
                    </Switch>

                </div>
            }

        </div>
    )
}

const EventHeader = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Nav = styled.div`
    a {
        margin-right: 10px;
    }
`;

const EventDate = styled.div`
    color: #61727C;
    font-size: 15px;
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 5px;

    & > * {
        margin-right: 5px;
    }
`;