import * as React from 'react';
import styled from 'styled-components';
import { EventsScreen } from './events_screen';
import { HashRouter as Router, Link, Switch, Route } from 'react-router-dom';
import { EventDetailScreen } from './event_detail_screen';
import TeamUpLogo from './images/logo.svg';
import HugoSearching from './images/hugo-searching.svg';
import Brain from './images/brain.svg';
import { ProfileScreen } from './profile_screen';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { IconButton } from '@material-ui/core';

interface IProps {
    user: firebase.User
    firebaseApp: firebase.app.App;
}

export const AuthenticatedApp = (props: IProps) => {
    const {
        user,
        firebaseApp,
    } = props;

    return (
        <Router>
            <AppScreen>
                <Header>
                    <Link to="/">Events</Link>
                    <Spacer />
                    <HeaderLogo src={TeamUpLogo} />
                    <Spacer />
                    <Link to="/myprofile">
                        <Avatar style={{ backgroundImage: `url(${user.photoURL})` }} />
                    </Link>
                    <IconButton onClick={() => firebaseApp.auth().signOut()} style={{ color: 'white' }}>
                        <ExitToAppIcon />
                    </IconButton>
                </Header>
                <Page>
                    <Switch>
                        <Route path="/events/:eventId">
                            <>
                                <MainContent>
                                    <EventDetailScreen
                                        firebaseApp={firebaseApp}
                                    />
                                </MainContent>
                            </>
                        </Route>
                        <Route path="/myprofile">
                            <>
                                <SecondaryContent />
                                <MainContent>
                                    <ProfileScreen
                                        firebaseApp={firebaseApp}
                                        firebaseUser={user}
                                    />
                                </MainContent>
                            </>
                        </Route>
                        <Route path="/">
                            <>
                                <SecondaryContent>
                                    <img src={HugoSearching} />
                                </SecondaryContent>
                                <MainContent style={{ maxHeight: 467 }}>
                                    <EventsScreen
                                        firebaseUser={user}
                                        firebaseApp={firebaseApp}
                                    />
                                </MainContent>
                            </>
                        </Route>
                    </Switch>
                </Page>
            </AppScreen>
        </Router>

    );
};

export const AppScreen = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    background: radial-gradient(153.78% 153.78% at 50% 0%, #8B83E2 0%, #69ACCF 70.9%, #FFFFFF 100%);
`;

export const Spacer = styled.div`
    flex: 1;
`;

export const Header = styled.div`
    min-height: 5vh;
    color: white;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 0.15em 1em;

    img {
        max-height: 25px;
        margin: 5px;
    }

    a {
        text-decoration: none;
        font-weight: bold;
        color: white;
        margin: 5px;
    }
`;

export const SignOutButton = styled.button`
    outline: none;
    background: none;
    border: none;
    cursor: pointer;
    color: inherit;
    font-family: inherit;
    font-size: inherit;
    border-radius: 5px;

    :hover {
        background-color: rgba(255, 255, 255, 0.2)
    }
`;

export const Page = styled.div`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

export const MainContent = styled.div`
    flex: 1;
    max-width: 550px;
    min-width: 350px;
    max-height: 650px;
    overflow: hidden;
    background: #FFFFFF;
    border-radius: 12px;
    margin-top: 100px;
    margin-left: 100px;
    margin-right: 100px;
    padding: 38px;
    display: flex;
    flex-direction: column;
`;

export const SecondaryContent = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative; 

    img {
        max-width: 80%;
        max-height: 80%;
    }
`;

export const HeaderTitle = styled.div`
    margin: 10px;
`;

export const HeaderLogo = styled.img`

`;

const Avatar = styled.div`
    width: 30px;
    height: 30px;
    border-radius: 15px;
    background-size: cover;
`;