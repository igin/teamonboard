import * as React from 'react';
import firebase from 'firebase/app';
import * as firebaseui from 'firebaseui';
import styled from 'styled-components';
import { AppScreen, Header, Spacer, HeaderLogo, SignOutButton, Page, MainContent, SecondaryContent } from './authenticated_app';
import { Link } from 'react-router-dom';
import TeamUpLogo from './images/logo.svg';
import HugoSearching from './images/hugo-searching.svg';

interface IProps {
    firebaseUI: firebaseui.auth.AuthUI;
}

var uiConfig = {
    signInSuccessUrl: '/teamonboard/',
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
    ],
    tosUrl: '/teamonboard/index.html#terms-of-services',
    privacyPolicyUrl: '/teamonboard/index.html#privacy-policy'
};


export const SignIn = (props: IProps) => {
    const {
        firebaseUI,
    } = props;

    React.useEffect(() => {
        firebaseUI.start('#firebase-ui-container', uiConfig);
    }, []);

    return (
        <AppScreen>
            <Header>
                <Spacer />
                <HeaderLogo src={TeamUpLogo} />
                <Spacer />
            </Header>
            <Page>
                <SecondaryContent>
                    <img src={HugoSearching} />
                </SecondaryContent>
                <MainContent>
                    <h1>Join the movement and hack with us.</h1>
                    <p>Sign in below to get access to all our glorious events.</p>
                    <p>{firebaseUI.isPendingRedirect() ? 'Signing you in ...' : null}</p>
                    <SignInContainer>
                        <div id="firebase-ui-container">
                        </div>
                    </SignInContainer>
                </MainContent>
            </Page>
        </AppScreen>
    );
};

const FullPageDialog = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    img {
        max-width: 150px;
    }
`;

const SignInContainer = styled.div`
    height: 150px;
`;