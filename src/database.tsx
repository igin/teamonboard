
export interface IdeationEventData {
    title: string;
    description: string;
    created_date: Date;
    start_date: Date;
    end_date: Date;
    tags: String[];
}

export interface IdeationEvent extends IdeationEventData {
    id: string;
}

export interface EventJoin {
    eventId: string;
    userId: string;
}

export interface UserProfile {
    id: string;
    name: string;
    description: string;
    joined_events: string[];
    skills: Skill[];
}

export interface Skill {
    name: string;
    level: number;
}

export interface IdeaData {
    title: string;
    pitch: string;
    owner: string;
    eventId: string;
}

export interface Idea extends IdeaData {
    id: string;
}

export class Database {
    db: firebase.firestore.Firestore;

    constructor(firebaseApp: firebase.app.App) {
        this.db = firebaseApp.firestore()
    }

    async getOrCreateProfile(user: firebase.User): Promise<UserProfile> {
        const profileSnapshot = await this.db.collection('profiles').doc(user.uid).get();

        if (profileSnapshot.exists) {
            return profileSnapshot.data() as UserProfile;
        } else {
            const newProfile: UserProfile = {
                id: user.uid,
                name: user.displayName,
                description: "...",
                joined_events: [],
                skills: []
            };
            this.db.collection('profiles').doc(newProfile.id).set(newProfile);
            return newProfile;
        }
    }

    async createEvent(event: IdeationEventData, ownerUid: string): Promise<IdeationEvent> {
        const eventSnapshot = await this.db.collection('events').add(event);
        return {
            ...event,
            id: eventSnapshot.id
        };
    }

    onAllEventsChanged(callback: (events: IdeationEvent[]) => void): () => void {
        this.db.collection('events').orderBy('start_date', 'desc').onSnapshot((eventsSnapshot) => {
            const allEvents = eventsSnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as IdeationEvent));
            callback(allEvents);
        })

        return () => { };
    }

    onJoinedEventsChanged(uid: string, callback: (events: IdeationEvent[]) => void): () => void {
        this.db.collection('event_joins').where('userId', '==', uid).onSnapshot((joinsSnapshot) => {
            const joinedEventsPromises = joinsSnapshot.docs.map((doc) => this.db.collection('events').doc(doc.data().eventId).get());
            Promise.all(joinedEventsPromises)
                .then((joinedEventSnapshots) => {
                    const joinedEvents = joinedEventSnapshots.map((snapshot) => ({
                        id: snapshot.id,
                        ...snapshot.data()
                    }) as IdeationEvent);
                    callback(joinedEvents);
                });
        });
        return () => { }
    }

    joinEvent(eventId: string, uid: string) {
        this.db.collection('event_joins').add({
            eventId: eventId,
            userId: uid
        });
    }

    async getEvent(eventId: string): Promise<IdeationEvent> {
        const event = await this.db.collection('events').doc(eventId).get();
        return {
            id: event.id,
            ...event.data()
        } as IdeationEvent;
    }

    unJoinEvent(eventId: string, uid: string) {
        this.db.collection('event_joins').where('eventId', '==', eventId).where('userId', '==', uid).get()
            .then((snapshot) => {
                snapshot.docs.forEach((doc) => {
                    this.db.collection('event_joins').doc(doc.id).delete();
                })
            })
    }

    onIdeasInEvent(eventId: string, callback: (ideas: Idea[]) => void) {
        console.log('listen for ideas in event ' + eventId)
        this.db.collection('ideas').where('eventId', '==', eventId).onSnapshot((snapshot) => {
            const ideas = snapshot.docs.map((doc) => ({
                id: doc.id,
                ...doc.data()
            }) as Idea);
            callback(ideas);
        });
        return () => { };
    }

    createIdeaInEvent(idea: IdeaData) {
        this.db.collection('ideas').add(idea);
    }
}
