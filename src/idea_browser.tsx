import * as React from 'react';
import { Idea, Database } from './database';
import { useParams } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { Card, CardWrapper } from 'react-swipeable-cards';
import styled from 'styled-components';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import IconButton from '@material-ui/core/IconButton';
import Zap from './images/zap.svg';
import Target from './images/target.svg';

interface IProps {
    firebaseApp: firebase.app.App
}

export const IdeaBrowser = (props: IProps) => {
    const { firebaseApp } = props;
    const [ideas, setIdeas] = React.useState<Idea[]>([]);
    const [currentIdeaIndex, setCurrentIdeaIndex] = React.useState<number>(0);
    const { eventId } = useParams();

    const [upvotedIdeas, setUpvotedIdeas] = React.useState<string[]>([]);
    const [downvotedIdeas, setDownvotedIdeas] = React.useState<string[]>([]);

    const db = new Database(firebaseApp);

    const getData = () => {
        db.onIdeasInEvent(eventId, (ideas) => {
            console.log('new ideas arrived', ideas)
            setIdeas(ideas);
        })
    };

    React.useEffect(() => {
        getData();
    }, []);

    return (
        <div>
            <IdeaStack>
                <IdeaContainer>
                    <p>You looked through all submitted ideas. Do you have any ideas that you want to propose yourself?</p>
                    <Button onClick={() => {
                        db.createIdeaInEvent({
                            eventId,
                            title: "New Idea",
                            pitch: "This a glorious idea that nobody else could ever think of. Join me!",
                            owner: ""
                        })
                    }}>
                        Submit new Idea
                    </Button>
                </IdeaContainer>
                {ideas.map((currentIdea) => {
                    const isUpvoted = upvotedIdeas.indexOf(currentIdea.id) !== -1;
                    const isDownvoted = downvotedIdeas.indexOf(currentIdea.id) !== -1;
                    const className = isUpvoted ? 'upvoted' : (isDownvoted ? 'downvoted' : '');

                    return (<IdeaContainer key={currentIdea.id} className={className}>
                        <img src={Zap} style={{ height: 20 }} />
                        <IdeaTitle>{currentIdea.title}</IdeaTitle>
                        <IdeaPitch>{currentIdea.pitch}</IdeaPitch>
                        <div style={{ flex: 1 }}></div>
                        <div style={{ display: 'flex', flexDirection: 'row', marginTop: 15 }}><img src={Target} style={{ marginRight: 5 }} />SKILLS REQUIRED</div>
                        <p>Frontend Developer | UX Designer</p>
                        <IdeaActions>
                            <IconButton onClick={() => {
                                setDownvotedIdeas([...downvotedIdeas, currentIdea.id]);
                            }}>
                                <ThumbDownIcon />
                            </IconButton>
                            <LearnMoreButton>Learn More</LearnMoreButton>
                            <IconButton onClick={() => {
                                setUpvotedIdeas([...upvotedIdeas, currentIdea.id]);
                            }}>
                                <ThumbUpIcon />
                            </IconButton>
                        </IdeaActions>
                    </IdeaContainer>);
                })}

            </IdeaStack>

        </div>
    );
};

const IdeaStack = styled.div`
    position: relative;
    height: 500px;
    align-self: stretch;
`;

const IdeaContainer = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 400px;
    background-color: white;
    border: 2px solid #AAC1CE;
    border-radius: 12px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px;
    transition: transform 1s;    

    &.upvoted {
        transform: translateX(1000px) rotate(45deg);
    }

    &.downvoted {
        transform: translateX(-1000px) rotate(-45deg);
    }

`;

const IdeaTitle = styled.h2`
    margin-bottom: 1em;
    text-align: center;
`;

const IdeaPitch = styled.div`
    font-size: medium;
    line-height: 2;
`;

const IdeaActions = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
`;

const LearnMoreButton = styled(Button)`
    border: 2px solid gray;
`;