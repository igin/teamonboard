import * as React from 'react';
import styled from 'styled-components';
import CheckIcon from '@material-ui/icons/Check';
import ToggleButton from '@material-ui/lab/ToggleButton';
import { IdeationEvent, UserProfile, Database } from './database';
import { Button } from '@material-ui/core';
import moment from 'moment';
import { Link } from 'react-router-dom';
import Brain from './images/brain.svg';
import EventIcon from '@material-ui/icons/Event';

const DateFormat = 'MMMM Do YYYY, h:mm a';

interface IProps {
    firebaseApp: firebase.app.App;
    firebaseUser: firebase.User;
}


export const EventsScreen = (props: IProps) => {
    const {
        firebaseApp,
        firebaseUser
    } = props;

    const [events, setEvents] = React.useState<IdeationEvent[]>([]);
    const [joinedEvents, setJoinedEvents] = React.useState<IdeationEvent[]>([]);
    const joinedEventIds = joinedEvents.map((event) => event.id);
    const [profile, setProfile] = React.useState<UserProfile | null>(null);

    const database = new Database(firebaseApp);

    const getData = (): () => void => {
        database.getOrCreateProfile(firebaseUser).then((profile) => {
            console.log(profile);
            setProfile(profile);
        });

        database.onAllEventsChanged((allEvents) => {
            setEvents(allEvents);
        })

        database.onJoinedEventsChanged(firebaseUser.uid, (eventJoins) => {
            setJoinedEvents(eventJoins);
        });

        const unsubscribe = () => { };
        return unsubscribe;
    };

    React.useEffect(() => {
        const unsubscribe = getData();
        return unsubscribe;
    }, []);

    return (
        <>
            <h1>Welcome back!</h1>
            <p>Please choose an event from the list below to join.</p>
            <div style={{ flex: 1, overflowY: 'scroll' }}>
                {events.map((event) => {
                    console.log(joinedEventIds)
                    console.log(event);
                    const eventJoinsForEvent = joinedEventIds.filter((eventId) => eventId === event.id);
                    const eventJoined = eventJoinsForEvent.length > 0;
                    return (
                        <Link to={'/events/' + event.id}>
                            <EventCard key={event.id}>
                                <EventLogo>
                                    <img src={Brain} />
                                </EventLogo>
                                <EventDetails>
                                    <EventTitle>{event.title}</EventTitle>
                                    <EventDate><EventIcon style={{ fontSize: 'medium', opacity: 0.5 }} />{moment(event.start_date.toDate()).format(DateFormat)}</EventDate>
                                </EventDetails>
                            </EventCard>
                        </Link>
                    )
                })}
            </div>
        </>
    );
};

const EventCard = styled.div`
    display: flex;
    flex-direction: row;
    cursor: pointer;
    padding: 6px;
    border-radius: 5px;
    &:hover {
        background: rgba(120,153, 215, 0.5);
        h3 {
            color: white;
        }
    }
`;

const EventLogo = styled.div`
    width: 67px;
    height: 67px;
    border: 2px solid #789AD7;
    border-radius: 12px;
    display: flex;
    align-items: center;
    justify-content: center;

    img {
        max-width: 56px;
        max-height: 56px;
    }
`;

const EventDetails = styled.div`
    flex: 1;
    margin: 10px;
`;

const EventTitle = styled.h3`
    margin: 0;
    font-size: 16px;
    line-height: 23px;
`;

const EventDate = styled.div`
    color: #61727C;
    font-size: 15px;
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 5px;

    & > * {
        margin-right: 5px;
    }
`;

const EventActions = styled.div`

`;